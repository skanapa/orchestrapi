from string import Template
import os, shlex, time, subprocess

class OrchestrAPI():
	def __init__(self, name, parent_path, parent_name, hostname, verbose=0):
		self.name = name
		self.hostname = hostname
		self.verbose = verbose

		self.working_path = parent_path + '/' + parent_name + "_" + str(os.getpid())
		self.path = self.working_path + '/' + self.name.replace(' ','_')
		self.file_pid_cmd = self.path + '/pid-'
		self.rsync_dico = []
		self.ssh_username = "root"
		self.ssh_template = Template("ssh $user@$hostname '$cmd_str'")
		self.rsync_template = Template("rsync -rvh $rsync_source $user@$hostname:$rsync_destination")
		self.rsync_local_template = Template("rsync -rvh $rsync_source $rsync_destination")
		self.script_template = Template(
"""#!/bin/bash
$export_var
echo -e "" | tee -a $stdoutfile >> $stderrfile
echo -e "" | tee -a $stdoutfile >> $stderrfile
date | tee -a $stdoutfile >> $stderrfile # writing the date in the log files
echo -e "" | tee -a $stdoutfile >> $stderrfile
sh -c 'echo $$$$ > $pidfile && exec nohup $cmd' 1>> $stdoutfile 2>> $stderrfile & disown
""" # TODO : foolproof le $cmd
		) # In the string template, '$$$$' is transformed in '$$' (which is the variable that contain the pid of the process)

		if self.verbose > 4:
			print(self.name + " :: Creating working directory.")
		if os.path.isdir(self.path) == False:
			os.makedirs(self.path)
		else:
			if self.verbose > 4:
				print(self.name + " :: Working directory already created.")


	def get_path(self):
		return self.path


	def get_hostname(self):
		return self.hostname

		# TODO : change script and scripted to something like wrapper and wrapped
	def write_script(self, var_export, cmdline, filename="script.sh", gen_pid_file=True):
		self.script_filename = self.path + '/' + filename

		if self.verbose > 4:
			print(self.name + " :: Writing script to file.")

		if self.verbose > 5:
			print(self.name + " :: script file path :" + self.script_filename)

		stdoutscript = self.path + '/stdout'
		stderrscript = self.path + '/stderr'
		if os.path.isfile(stdoutscript) == False:
			open(stdoutscript, 'x')
		if os.path.isfile(stderrscript) == False:
			open(stderrscript, 'x')

		script_pid = self.file_pid_cmd + filename
		if gen_pid_file == True:
			if os.path.isfile(script_pid) == False:
				open(script_pid, 'x')
				script_string = self.script_template.substitute(stdoutfile=stdoutscript, stderrfile=stderrscript, pidfile=script_pid, cmd=cmdline, export_var=var_export)
		else:
			script_string = self.script_template.substitute(stdoutfile=stdoutscript, stderrfile=stderrscript, pidfile="/dev/null", cmd=cmdline, export_var=var_export)

		if self.verbose > 5:
			print(self.name + " :: script :\n" + script_string)

		script_file = open(self.script_filename, 'w')
		script_file.write(script_string)
		script_file.close()

		return self.script_filename

	def async_files_transfer(self, source, destination, rsync_queue=None):
		if self.verbose > 4:
			print(self.name + " :: Creating dictionary for files to rsync.")
		dictio = {'Source' : source, 'Destination' : destination}

		if rsync_queue == None: # if the queue is that provided by the API
			self.rsync_dico.append(dictio)
		else: # if the queue is provided by the user (to use one queue across several components)
			rsync_queue.append(dictio)

		if self.verbose > 5:
			print(self.name + "  :: Appending to list the dictionary :\n" + dictio)


	def flush_files_transfer(self):
		if self.verbose > 4:
			print(self.name + " :: Flush, transfering all files.")
		if self.verbose > 5:
			print(self.name + " :: RSYNC on this list of dictionaries:\n" + self.rsync_dico)
		for n in self.rsync_dico:
			self.transfer_files(n['Source'],n['Destination'])
		if self.verbose > 4:
			print(self.name + " :: Emptying list of dictionaries.")
		self.rsync_dico = []

	def transfer_files(self, source, destination):
		if self.verbose > 4:
			print(self.name + " :: Syncing files.")

		if os.path.isdir(self.path) == True:
		#### IF
			log_rsync_path = self.path + "/rsync"
			if os.path.isdir(log_rsync_path) == False:
				os.mkdir(log_rsync_path)

			outname = log_rsync_path + "/stdout"
			errname = log_rsync_path + "/stderr"
			fout = open(outname, 'w')
			ferr = open(errname, 'w')
			log_mkdir_path = log_rsync_path + "/mkdir"
			if os.path.isdir(log_mkdir_path) == False:
				os.mkdir(log_mkdir_path)

			mkdir_outname = log_mkdir_path + "/stdout"
			mkdir_errname = log_mkdir_path + "/stderr"
			mkdir_file_stdout = open(mkdir_outname, 'w')
			mkdir_file_stderr = open(mkdir_errname, 'w')

			if self.hostname == 'localhost':
				if os.path.isdir(destination) == False:
					if self.verbose > 4:
						print(self.name + " :: Creating destination directory.")
					os.makedirs(destination)

				rsync_local = self.rsync_local_template.substitute(rsync_source=source, rsync_destination=destination)
				if self.verbose > 5:
					print(self.name + " :: Rsync local command : " + rsync_local)

				p1 = subprocess.Popen(shlex.split(rsync_local), stdout=fout, stderr=ferr, cwd=self.path)
				p1.wait()
			else:
				if self.verbose > 4:
					print(self.name + " :: Creating remote destination directory.")

				distant_mkdir = self.ssh_template.substitute(user=self.ssh_username,hostname=self.hostname, cmd_str="mkdir -vp "+destination)
				p2 = subprocess.Popen(shlex.split(distant_mkdir), stdout=mkdir_file_stdout, stderr=mkdir_file_stderr, cwd=self.path)
				p2.wait()
				if self.verbose > 5:
					print(self.name + " :: Rsync distant command : " + distant_mkdir)

				rsync_distant = self.rsync_template.substitute(rsync_source=source, user=self.ssh_username, hostname=self.hostname, rsync_destination=destination)

				if self.verbose > 5:
					print(rsync_distant)

				p3 = subprocess.Popen(shlex.split(rsync_distant), stdout=fout, stderr=ferr, cwd=self.path)
				p3.wait()
		#### ELSE
		else:
			print(self.name + " :: ERROR" + self.path + "Isn't a directory.")
			quit()


	def _launch_process(self, cmdline):
		if os.path.isdir(self.path) == True:
			outname = self.path + "/Popen.stdout" # creating the path of the files
			errname = self.path + "/Popen.stderr"
			fout = open(outname, 'w')
			ferr = open(errname, 'w')
			if self.verbose > 5:
				print(self.name + " :: Command is : " + cmdline)

			p = subprocess.Popen(shlex.split(cmdline), stdout=fout, stderr=ferr, cwd=self.path)
			return p
		else:
			print(self.name + ":: ERROR" + self.path + "Isn't a directory.")
			quit()


	def run_cmd(self, cmd):
		if self.verbose > 4:
			print(self.name + " :: Executing command.")

		if self.hostname == 'localhost':
			return self._launch_process(cmd)
		else:
			ssh_cmd = self.ssh_template.substitute(user=self.ssh_username, hostname=self.hostname, cmd_str=cmd)
			return self._launch_process(ssh_cmd)


	def kill_scripted_cmd(self, scriptname='script.sh', sig="15"):
		file_script_pid = self.file_pid_cmd + scriptname

		if self.verbose > 4:
			print(self.name + " :: Killing process.")
		kill_cmd = """bash -c "kill -s """ + sig + """ $(cat """ + file_script_pid + """)" """

		if self.hostname == 'localhost':
			if self.verbose > 5:
				print(self.name + " :: kill command : " + kill_cmd)
			subprocess.Popen(shlex.split(kill_cmd))
		else:
			ssh_kill_cmd = self.ssh_template.substitute(user=self.ssh_username, hostname=self.hostname, cmd_str=kill_cmd)
			if self.verbose > 5:
				print(self.name + " :: kill command : " + ssh_kill_cmd)
			subprocess.Popen(shlex.split(ssh_kill_cmd))
